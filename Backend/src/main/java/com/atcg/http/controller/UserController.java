package com.atcg.http.controller;

import com.atcg.config.SecurityUtility;
import com.atcg.http.domain.User;
import com.atcg.http.domain.security.PasswordResetToken;
import com.atcg.http.domain.security.Role;
import com.atcg.http.domain.security.UserRole;
import com.atcg.http.email.MailConstructor;
import com.atcg.http.service.UserSecurityService;
import com.atcg.http.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;

/**
 * Created by szefo on 07/03/17.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final UserSecurityService userSecurityService;
    private final MailConstructor mailConstructor;
    private final JavaMailSender mailSender;

    @Autowired
    public UserController(UserService userService,
                          UserSecurityService userSecurityService,
                          MailConstructor mailConstructor,
                          JavaMailSender mailSender) {
        this.userService = userService;
        this.userSecurityService = userSecurityService;
        this.mailConstructor = mailConstructor;
        this.mailSender = mailSender;
    }

    @RequestMapping("/")
    public ResponseEntity<String> logout(@RequestParam("logout") String logout) {
        return new ResponseEntity<>("Logout success.", HttpStatus.OK);
    }

    @RequestMapping("/resource")
    @PreAuthorize("isAuthenticated()")
    public Map<String, Object> home() {
        final Map<String, Object> model = new HashMap<>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        return model;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Principal user(Principal user) {
        return user;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody User user) {
        userService.save(user);
    }

    @RequestMapping("/checkSession")
    public String checkSession() {
        return "Session Active";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout() {
        SecurityContextHolder.clearContext();
        return "logout success.";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) {
        return userService.save(user);
    }

    @RequestMapping(value = "/newUser", method = RequestMethod.POST)
    public ResponseEntity<String> newUser(HttpServletRequest request, @RequestBody Map<String, String> mapper) throws Exception {
        final String username = mapper.get("username");
        final String userEmail = mapper.get("email");

        if (Optional.ofNullable(userService.findByUsername(username)).isPresent()) {
            return new ResponseEntity<>("usernameExists", HttpStatus.BAD_REQUEST);
        }

        if (Optional.ofNullable(userService.findByEmail(userEmail)).isPresent()) {
            return new ResponseEntity<>("emailExists", HttpStatus.BAD_REQUEST);
        }

        final String password = SecurityUtility.randomPassword();
        final String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
        final Role role = new Role(1, "ROLE_USER");
        final User user = new User(username, userEmail, encryptedPassword);
        final Set<UserRole> userRoles = new HashSet<>();
        final String token = UUID.randomUUID().toString();
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        final SimpleMailMessage email = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user, password);

        mailSender.send(email);

        userRoles.add(new UserRole(user, role));
        userService.createUser(user, userRoles);
        userService.createPasswordResetTokenForUser(user, token);

        return new ResponseEntity<>("User Added Successfully!", HttpStatus.OK);
    }

    @RequestMapping("/addNewUser")
    public ResponseEntity<String> addNewUser(Locale locale, @RequestParam("token") String token) {
        final PasswordResetToken passToken = userService.getPasswordResetToken(token);
        if (passToken == null) {
            return new ResponseEntity<>("Can't Add User!", HttpStatus.BAD_REQUEST);
        }

        final Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return new ResponseEntity<>("Can't Add User!", HttpStatus.BAD_REQUEST);
        }
        final User user = passToken.getUser();
        final String username = user.getUsername();
        final UserDetails userDetails = userSecurityService.loadUserByUsername(username);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails,
                userDetails.getPassword(),
                userDetails.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return new ResponseEntity("User Added Successfully!", HttpStatus.OK);
    }

    @RequestMapping("/forgetPassword")
    public ResponseEntity forgetPassword(@RequestBody Map<String, String> email, HttpServletRequest request) {
        LOGGER.debug(email.toString());
        final User user = userService.findByEmail(email.get("email"));
        if (user == null) {
            return new ResponseEntity("Email not found!", HttpStatus.BAD_REQUEST);
        }
        final String password = SecurityUtility.randomPassword();
        final String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
        final String token = UUID.randomUUID().toString();
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        final SimpleMailMessage newEmail = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user, password);

        LOGGER.error("password={}", password);
        LOGGER.error("encryptedPassword={}", encryptedPassword);
        user.setPassword(encryptedPassword);
        userService.save(user);
        userService.createPasswordResetTokenForUser(user, token);
        mailSender.send(newEmail);

        return new ResponseEntity("Email sent!", HttpStatus.OK);
    }

    @RequestMapping("/test")
    public String test() {
        return "test";
    }

    @RequestMapping("/token")
    @ResponseBody
    public Map<String,String> token(HttpSession session, HttpServletRequest request) {
        final String remoteHost = request.getRemoteHost();
        final int portNumber = request.getRemotePort();
        LOGGER.info("remote host={}", remoteHost);
        LOGGER.info("remote address={}", request.getRemoteAddr());
        LOGGER.info(remoteHost+":"+portNumber);

        return Collections.singletonMap("token", session.getId());
    }

}
