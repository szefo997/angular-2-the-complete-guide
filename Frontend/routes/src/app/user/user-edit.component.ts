import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ComponentCanDeactivate} from "./user-edit.guard";
import {Observable} from "rxjs";

@Component({
  selector: 'app-user-edit',
  template: `
    <h1>User edit</h1>
    <button (click)="done = true">Done</button>
    <button class="btn btn-alert" (click)="onNavigate()" >Go home</button>
   `
})
export class UserEditComponent implements OnInit, ComponentCanDeactivate {
  done: boolean = false;

  constructor(private router: Router) {}

  ngOnInit() {}

  onNavigate() {this.router.navigate(['/']);}

  canDeactivate(): Observable<boolean> | boolean {
    if(!this.done){
      return confirm('Do you want to leave ?');
    }
    return true;
  }

}
