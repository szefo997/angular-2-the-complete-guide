import {
  Component,
  OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy,
  Input, ViewChild, ContentChild
} from "@angular/core";
import {log} from "util";

@Component({
  selector: 'fa-life-cycle',
  template: `
    <ng-content></ng-content>
    <hr>
    <p #boundParagraph>{{bindable}}</p>
    <p>{{boundParagraph.textContent}}</p>
  `,
  styles: []
})
export class LifeCycleComponent implements OnChanges,
  OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked,
  AfterViewInit, AfterViewChecked, OnDestroy {

  @Input()
  bindable = 1000;

  @ViewChild('boundParagraph')
  boundParagraph;

  @ContentChild('boundContent')
  boundContent: HTMLElement;

  constructor() {
  }

  ngAfterContentChecked(): void {
    log('ngAfterContentChecked');
  }

  ngOnChanges(): void {
    log('ngOnChanges');
  }

  ngDoCheck(): void {
    log('ngDoCheck');
  }

  ngAfterContentInit(): void {
    log('ngAfterContentInit');
    console.log(this.boundContent);
  }

  ngAfterViewChecked(): void {
    log('ngAfterViewChecked');
  }

  ngAfterViewInit(): void {
    log('ngAfterViewInit');
    console.log(this.boundParagraph);
  }

  ngOnDestroy(): void {
    log('ngOnDestroy');
  }

  ngOnInit(): void {
    log('ngOnInit');
  }

  private log(hook: string) {
    console.log(hook);
  }

}
