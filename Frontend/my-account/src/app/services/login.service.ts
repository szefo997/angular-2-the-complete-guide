import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {AppConst} from "../constants/app-const";

@Injectable()
export class LoginService {
  private serverPath: string = AppConst.serverPath;

  constructor(private http: Http) {
  }

  sendCredential(username: string, password: string) {
    let url = this.serverPath + "/api/user/token";
    let encodedCredentials = btoa(username + ":" + password);
    console.log(encodedCredentials);
    let basicHeader = "Basic " + encodedCredentials;
    console.log(basicHeader);
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': basicHeader
      // 'Access-Control-Allow-Credentials' : true
    });
    console.log(headers);
    return this.http.get(url, {headers: headers});
  }

  checkSession() {
    let url = this.serverPath + "/api/user/checkSession";
    let tokenHeader = new Headers({
      'x-auth-token': localStorage.getItem("xAuthToken")
    });
    return this.http.get(url, {headers: tokenHeader});
  }

  logout() {
    let url = this.serverPath + "/api/user/logout";
    let tokenHeader = new Headers({
      'x-auth-token': localStorage.getItem("xAuthToken")
    });
    return this.http.post(url, '', {headers: tokenHeader});
  }

  test(){
    let url = this.serverPath + "/api/user/resource";
    let tokenHeader = new Headers({
      'x-auth-token': localStorage.getItem("xAuthToken")
    });
    return this.http.get(url, {headers: tokenHeader});
  }

}
