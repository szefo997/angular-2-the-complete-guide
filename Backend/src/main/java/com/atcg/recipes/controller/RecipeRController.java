package com.atcg.recipes.controller;

import com.atcg.recipes.domain.Ingredient;
import com.atcg.recipes.domain.Recipe;
import com.atcg.recipes.service.IngredientService;
import com.atcg.recipes.service.RecipeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by szefo on 11/03/17.
 */
@RestController
@RequestMapping("/api")
public class RecipeRController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeRController.class);
    private final RecipeService recipeService;
    private final IngredientService ingredientService;

    @Autowired
    public RecipeRController(RecipeService recipeService, IngredientService ingredientService) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
    }

    @RequestMapping(value = "/recipes", method = RequestMethod.GET)
    public Iterable findAll() {
        return recipeService.findAll();
    }

    @RequestMapping(value = "/recipe/save", method = RequestMethod.POST)
    public Recipe save(@RequestBody Recipe recipe) {
        return recipeService.save(recipe);
    }

    @RequestMapping(value = "/recipes/save", method = RequestMethod.PUT)
    public void save(@RequestBody Iterable<Recipe> entities) {
        LOGGER.error("SAVE entities={}", entities.toString());
        entities.forEach(recipe -> {
            final Recipe savedRecipe = recipeService.save(recipe);
            final List<Ingredient> ingredients = recipe.getIngredients();
            ingredients.forEach(ingredient -> {
                        ingredient.setRecipeId(savedRecipe.getRecipeId());
                        ingredientService.save(ingredient);
                    }
            );
        });
    }

    @RequestMapping(value = "/recipe/{recipeId}/delete", method = RequestMethod.DELETE)
    public void delete(@PathVariable long recipeId) {
        recipeService.delete(recipeId);
    }

}
