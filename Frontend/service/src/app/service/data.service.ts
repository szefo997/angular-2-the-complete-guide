import {Injectable, EventEmitter} from "@angular/core";

import {LogService} from "./log.service";

@Injectable()
export class DataService {
  push = new EventEmitter<string>();
  private data: string[] = [];

  constructor(private logService: LogService) {}

  addData(string: string) {
    this.data.push(string);
    this.logService.writeToLog("Saved item: " + string)
  }

  getData() {
    return this.data;
  }

  pushData(value :string){
    this.push.emit(value);
  }

}
