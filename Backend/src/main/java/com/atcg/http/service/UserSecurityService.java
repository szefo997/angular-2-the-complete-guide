package com.atcg.http.service;

import com.atcg.config.SecurityUtility;
import com.atcg.http.domain.User;
import com.atcg.http.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSecurityService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserSecurityService.class);
    private final UserRepository userRepository;

    @Autowired
    public UserSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        LOGGER.error("EQUALS MATCHES ={}", SecurityUtility.passwordEncoder().matches("HZQEPTQEKT2ZLLW8QF", "$2a$12$udecp6.d7FrGEK3vaEeTi.1h0LP.46Uc5HFUW4xVoOyJiy6c0g7Xq"));
//        LOGGER.error("EQUALS MATCHES ={}", SecurityUtility.passwordEncoder().matches("HZQEPTQEKT2ZLLW8QF", SecurityUtility.passwordEncoder().encode("HZQEPTQEKT2ZLLW8QF")));
        return Optional.ofNullable(userRepository.findByUsername(username))
                .<UsernameNotFoundException>orElseThrow(() -> {
                    LOGGER.warn("Username {} not found", username);
                    throw new UsernameNotFoundException("Username " + username + " not found");
                });
    }


}