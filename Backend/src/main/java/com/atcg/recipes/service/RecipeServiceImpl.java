package com.atcg.recipes.service;

import com.atcg.recipes.dao.RecipeDao;
import com.atcg.recipes.domain.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by szefo on 11/03/17.
 */
@Service
@Transactional
public class RecipeServiceImpl implements RecipeService<Recipe> {

    private final RecipeDao recipeDao;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public RecipeServiceImpl(RecipeDao recipeDao) {
        this.recipeDao = recipeDao;
    }

    @Override
    public <S extends Recipe> S save(S entity) {
        return recipeDao.save(entity);
    }

    @Override
    public <S extends Recipe> Iterable<S> save(Iterable<S> entities) {
        return recipeDao.save(entities);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Recipe> findAll() {
        return recipeDao.findAll();
    }

    @Override
    public void delete(Long aLong) {
        recipeDao.delete(aLong);
    }


}
