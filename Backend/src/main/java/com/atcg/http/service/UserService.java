package com.atcg.http.service;

import com.atcg.http.domain.User;
import com.atcg.http.domain.security.PasswordResetToken;
import com.atcg.http.domain.security.UserRole;

import java.util.Set;

/**
 * Created by szefo on 07/03/17.
 */
public interface UserService {

    <S extends User> S save(S entity);

    User findByUsername(String username);

    User findByEmail(String email);

    Iterable<User> findAll();

    void createPasswordResetTokenForUser(final User user, final String token);

    PasswordResetToken getPasswordResetToken(final String token);

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

}
