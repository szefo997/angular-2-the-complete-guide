package com.atcg.recipes.dao;

import com.atcg.recipes.domain.Ingredient;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by szefo on 11/03/17.
 */
public interface IngredientDao extends CrudRepository<Ingredient, Long> {

    <S extends Ingredient> Iterable<S> save(Iterable<S> entities);

    <S extends Ingredient> S save(S entity);

    Ingredient findOne(Long aLong);

    Iterable<Ingredient> findAllByRecipeId(Long recipeId);
}
