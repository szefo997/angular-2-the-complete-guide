package com.atcg.recipes.service;

import com.atcg.recipes.domain.Ingredient;

/**
 * Created by szefo on 11/03/17.
 */
public interface IngredientService<S> {

    <S extends Ingredient> Iterable<S> save(Iterable<S> entities);

    <S extends Ingredient> S save(S entity);

    Ingredient findOne(Long aLong);

    Iterable<Ingredient> findAllByRecipeId(Long recipeId);

}
