import {Component, OnInit} from "@angular/core";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styles: [`
    input.ng-invalid{
        border: 1px solid red;
    }
`]
})
export class TemplateDrivenComponent implements OnInit {

  user = {
    username: 'Marian',
    email: 'pazdzioch@wp.pl',
    password: 'asdasd',
    gender: 'male'
  };

  constructor() {
  }

  ngOnInit() {
  }

  genders = [
    'male',
    'female'
  ];

  onSubmit(form: NgForm) {
    console.log(form.value);
  }

}
