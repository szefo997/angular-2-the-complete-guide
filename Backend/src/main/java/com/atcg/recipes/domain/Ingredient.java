package com.atcg.recipes.domain;

import javax.persistence.*;

/**
 * Created by szefo on 10/03/17.
 */
@Entity
public final class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ingredientId;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false)
    private int amount;
    private Long recipeId;

    public Ingredient() {}

    public Ingredient(String name, int amount, Long recipeId) {
        this.name = name;
        this.amount = amount;
        this.recipeId = recipeId;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientId=" + ingredientId +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", recipeId=" + recipeId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (amount != that.amount) return false;
        if (ingredientId != null ? !ingredientId.equals(that.ingredientId) : that.ingredientId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return recipeId != null ? recipeId.equals(that.recipeId) : that.recipeId == null;
    }

    @Override
    public int hashCode() {
        int result = ingredientId != null ? ingredientId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (recipeId != null ? recipeId.hashCode() : 0);
        return result;
    }
}
