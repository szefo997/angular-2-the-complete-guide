import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-home',
  template: `
    <h1>home works!</h1>
    {{param}}
    `
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  param: string;

  constructor(private activatedRouter: ActivatedRoute) {
    this.subscription = activatedRouter.queryParams.subscribe(
      (queryParam: any) => this.param = queryParam['analytics']
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
