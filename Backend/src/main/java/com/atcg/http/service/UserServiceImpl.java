package com.atcg.http.service;

import com.atcg.http.domain.User;
import com.atcg.http.domain.security.PasswordResetToken;
import com.atcg.http.domain.security.UserRole;
import com.atcg.http.repository.PasswordResetTokenRepository;
import com.atcg.http.repository.RoleRepository;
import com.atcg.http.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;


/**
 * Created by szefo on 07/03/17.
 */
@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordResetTokenRepository passwordResetTokenRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
        passwordResetTokenRepository.save(new PasswordResetToken(token, user));
    }

    @Override
    @Transactional
    public User createUser(User user, Set<UserRole> userRoles) throws Exception {
        if (Optional.ofNullable(userRepository.findByUsername(user.getUsername())).isPresent()) {
            LOGGER.info("User with username {} already exist. Nothing will be done. ", user.getUsername());
            throw new Exception(String.format("User with username %s already exist. Nothing will be done. ", user.getUsername()));
        } else {
            for (UserRole ur : userRoles) {
                roleRepository.save(ur.getRole());
            }
            user.getUserRoles().addAll(userRoles);
        }
        return save(user);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(String token) {
        return passwordResetTokenRepository.findByToken(token);
    }
}
