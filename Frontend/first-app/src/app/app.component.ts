import {Component} from "@angular/core";

@Component({
  selector: 'app-root',
  template: `
    <h1>Inline template</h1>
    <fa-life-cycle *ngIf="!delete" [bindable]="boundValue">
        <p #boundContent>{{test}}</p>
    </fa-life-cycle>
    <button (click)="delete = true">delete</button>
    <button (click)="test = 'changed value'">change</button>
    <button (click)="boundValue = 2000">change binding</button>
  `,
  styles: [`
    h1 {color:red;}
  `]
})

export class AppComponent {
  delete = false;
  test = 'starting value';
  boundValue = 1000;
}
