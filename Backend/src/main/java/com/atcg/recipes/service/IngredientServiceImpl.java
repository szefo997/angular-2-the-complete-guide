package com.atcg.recipes.service;

import com.atcg.recipes.dao.IngredientDao;
import com.atcg.recipes.domain.Ingredient;
import com.atcg.recipes.domain.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by szefo on 11/03/17.
 */
@Service
@Transactional
public class IngredientServiceImpl implements IngredientService<Ingredient> {

    private final IngredientDao ingredientDao;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public IngredientServiceImpl(IngredientDao ingredientDao) {
        this.ingredientDao = ingredientDao;
    }

    @Override
    public <S extends Ingredient> Iterable<S> save(Iterable<S> entities) {
//        entities.forEach(recipe -> {
//            final Recipe savedRecipe = recipeService.save(recipe);
//            final List<Ingredient> ingredients = recipe.getIngredients();
//            ingredients.forEach(ingredient -> {
//                        ingredient.setRecipe(savedRecipe);
//                        ingredientService.save(ingredient);
//                    }
//            );
//        });
        em.getTransaction().begin();
        em.persist(entities);
        em.getTransaction().commit();

        return ingredientDao.save(entities);
    }

    @Override
    public <S extends Ingredient> S save(S entity) {
        return ingredientDao.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Ingredient findOne(Long aLong) {
        return ingredientDao.findOne(aLong);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Ingredient> findAllByRecipeId(Long recipeId) {
        return ingredientDao.findAllByRecipeId(recipeId);
    }

    }
