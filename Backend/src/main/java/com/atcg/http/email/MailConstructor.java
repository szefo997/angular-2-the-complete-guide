package com.atcg.http.email;

import com.atcg.http.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.messageresolver.IMessageResolver;
import org.thymeleaf.messageresolver.StandardMessageResolver;

import java.util.Locale;

/**
 * Created by z00382545 on 1/3/17.
 */
@Component
public class MailConstructor {

    private final Environment env;
    private final TemplateEngine templateEngine;
    private final IMessageResolver iMessageResolver = new StandardMessageResolver();

    @Autowired
    public MailConstructor(Environment env, TemplateEngine templateEngine) {
        this.env = env;
        this.templateEngine = templateEngine;
    }

    public SimpleMailMessage constructResetTokenEmail(String contextPath, Locale locale, String token, User user, String password) {
        final String message = "\nPlease use the following credentials to log in and edit your personal info including your own password. \nUsername: " + user.getUsername() + "\nPassword:" + password;
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("New User");
        email.setText(message);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }


}
