import {Component} from "@angular/core";
import {HttpService} from "./http.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  items: any[] = [];
  asyncString = this.httpService.getData();

  constructor(private httpService: HttpService) {
  }

  onSubmit(username: string, email: string) {
    var user = {username: username, email: email};

    console.log(user);

    this.httpService.sendData({username: username, email: email})
      .subscribe(
        data => console.log(data),
        error => console.log(error)
      );
  }

  onGetData() {
    return this.httpService.getOwnData()
      .subscribe(
        data => {
          // console.log(data);
          // const myArray = [];
          // for (let key in data) {
          //   myArray.push(data);
          // }
          this.items = data;
        }
      );
  }

}
