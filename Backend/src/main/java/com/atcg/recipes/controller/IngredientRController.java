package com.atcg.recipes.controller;

import com.atcg.recipes.domain.Ingredient;
import com.atcg.recipes.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by szefo on 11/03/17.
 */
@RestController
@RequestMapping("/api")
public class IngredientRController {

    private final IngredientService ingredientService;

    @Autowired
    public IngredientRController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @RequestMapping(value = "/ingredient/{ingredientId}", method = RequestMethod.GET)
    public Ingredient findAll(@PathVariable Long ingredientId){
        return ingredientService.findOne(ingredientId);
    }

    @RequestMapping(value = "/ingredients/{recipeId}", method = RequestMethod.GET)
    public Iterable findAllByIngredientRecipe(@PathVariable Long recipeId){
        return ingredientService.findAllByRecipeId(recipeId);
    }

    @RequestMapping(value = "/ingredient/save", method = RequestMethod.POST)
    public Ingredient save(@RequestBody Ingredient ingredient) {
        return ingredientService.save(ingredient);
    }

}
