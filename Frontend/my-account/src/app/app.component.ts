import {Component} from "@angular/core";
import {AppConst} from "./constants/app-const";
import {LoginService} from "./services/login.service";
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private serverPath = AppConst.serverPath;
  private loginError: boolean = false;
  private loggedIn = false;
  private credential = {'username': '', 'password': ''};

  private emailSent: boolean = false;
  private usernameExists: boolean = false;
  private emailExists: boolean = false;
  private username: string;
  private email: string;

  private emailNotExists: boolean = false;
  private forgetPasswordEmailSent: boolean = false;
  private recoverEmail: string;

  constructor(private loginService: LoginService, private userService: UserService) {
  }

  onLogin() {
    console.log(this.credential.username, this.credential.password);
    this.loginService.sendCredential(this.credential.username, this.credential.password).subscribe(
      res => {
        console.log(res);
        localStorage.setItem("xAuthToken", res.json().token);
        this.loggedIn = true;
        location.reload();
      }, error => {
        this.loggedIn = false;
        this.loginError = true;
        console.log(error);
      }
    );
  }

  onNewAccount() {
    this.usernameExists = false;
    this.emailExists = false;
    this.emailSent = false;

    console.log(this.username, this.email);

    this.userService.newUser(this.username, this.email).subscribe(
      res => {
        console.log(res);
        this.emailSent = true;
      },
      error => {
        console.log(error.text());
        let errorMessage = error.text();
        if (errorMessage === "usernameExists") this.usernameExists = true;
        if (errorMessage === "emailExists") this.emailExists = true;
      }
    );
  }

  onForgetPassword() {
    this.emailNotExists = false;
    this.forgetPasswordEmailSent = false;

    this.userService.retrievePassword(this.recoverEmail).subscribe(
      res => {
        console.log(res);
        this.forgetPasswordEmailSent = true;
      },
      error => {
        console.log(error.text());
        let errorMessage = error.text();
        if (errorMessage === "Email not found!") this.emailNotExists = true;
      }
    );
  }

  test() {
    this.loginService.test().subscribe(
      res => {
        console.log(res);
      }, error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn = true;
      },
      error => {
        this.loggedIn = false;
      }
    );
  }

}
