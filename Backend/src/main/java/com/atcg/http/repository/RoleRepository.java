package com.atcg.http.repository;

import com.atcg.http.domain.security.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by szefo on 21/03/17.
 */
public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findByName(String name);

}
