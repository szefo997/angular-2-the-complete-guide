import {Component} from "@angular/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [
    `.pipes{
        margin: 32px;
        padding: 32px;
     }
    `
  ]
})
export class AppComponent {
  myValue: string = 'lowercase';
  myDate: Date = new Date(2017, 2, 4);
  values = ['milk', 'bread', 'beans', 'sea'];

  asyncValues = new Promise(
    (resolve, reject) => {
      setTimeout(() => resolve("Data is here!"), 2000);
    }
  );
}
