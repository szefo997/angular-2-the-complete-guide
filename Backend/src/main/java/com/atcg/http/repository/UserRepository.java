package com.atcg.http.repository;

import com.atcg.http.domain.User;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by szefo on 07/03/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    <S extends User> S save(S entity);

    User findByUsername(String username);

    User findByEmail(String email);

    Iterable<User> findAll();

}
