package com.atcg.recipes.dao;

import com.atcg.recipes.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by szefo on 11/03/17.
 */
public interface RecipeDao extends CrudRepository<Recipe, Long> {

    <S extends Recipe> S save(S entity);

    <S extends Recipe> Iterable<S> save(Iterable<S> entities);

    Iterable<Recipe> findAll();

    void delete(Long aLong);

}
