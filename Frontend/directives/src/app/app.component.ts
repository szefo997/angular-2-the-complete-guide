import {Component} from '@angular/core';

@Component({
  selector: 'dir-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private toggle: boolean = true;
  private items = [1, 2, 3, 4, 5,];
  private value: number = 100;

  onSwitch() {
    this.toggle = !this.toggle;
  }
}
