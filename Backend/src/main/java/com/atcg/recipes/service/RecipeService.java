package com.atcg.recipes.service;

import com.atcg.recipes.domain.Recipe;

/**
 * Created by szefo on 11/03/17.
 */
public interface RecipeService<S> {

    <S extends Recipe> S save(S entity);

    <S extends Recipe> Iterable<S> save(Iterable<S> entities);

    Iterable<Recipe> findAll();

    void delete(Long aLong);

}
