import {Directive, HostListener, HostBinding, Input} from "@angular/core";
// import {Directive, ElementRef, Renderer} from "@angular/core";

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective {

  @Input() defaultColor: string = 'white';
  @Input('highlight') highlight: string = 'green';
  private backgroundColor: string;

  @HostListener('mouseenter')  mouseover() {
    this.backgroundColor = this.highlight;
  };

  @HostListener('mouseleave')  mouseleave() {
    this.backgroundColor = this.defaultColor;
  };

  @HostBinding('style.backgroundColor') get setColor() {
    return this.backgroundColor;
  };

  constructor() {
  }

  ngOnInit() {
    this.backgroundColor = this.defaultColor;
  }

  // constructor(private elementRef: ElementRef, private renderer: Renderer) {
  // this.elementRef.nativeElement.style.backgroundColor = 'green';
  // this.renderer.setElementStyle(this.elementRef.nativeElement, 'background-color', 'red');
  // }

}
