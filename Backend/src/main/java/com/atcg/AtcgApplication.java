package com.atcg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@EntityScan(basePackageClasses = {AtcgApplication.class, Jsr310JpaConverters.class})
@SpringBootApplication
public class AtcgApplication implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(AtcgApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AtcgApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Some action before run app");
    }

}
