import {Injectable, EventEmitter} from "@angular/core";
import {Recipe} from "./recipe";
import {Ingredient} from "../shared/ingridient";
import {Headers, Http, Response} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class RecipeService {

  recipesChanged =new EventEmitter<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe('Schnitzel', 'Very tasty',
      'http://www.daringgourmet.com/wp-content/uploads/2014/03/Schnitzel-7_edited.jpg', [
        new Ingredient('French Fries', 2),
        new Ingredient('Pork Meat', 1)
      ]),
    new Recipe('Summer Salad', 'Okayish',
      'http://ohmyveggies.com/wp-content/uploads/2013/06/the_perfect_summer_salad.jpg', [])
  ];

  constructor(private http: Http) {
  }

  getRecipes() {
    return this.recipes;
  }

  getRecipe(recipeIndex: number) {
    if (recipeIndex <= this.recipes.length) {
      return this.recipes[recipeIndex];
    } else {
      return null;
    }
  }

  deleteRecipe(recipe: Recipe) {
    this.recipes.splice(this.recipes.indexOf(recipe), 1);
  }

  editRecipe(oldRecipe: Recipe, newRecipe: Recipe) {
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe;
  }

  addRecipe(newRecipe: Recipe) {
    this.recipes.push(newRecipe);
  }

  storeData() {
    const body = JSON.stringify(this.recipes);
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    return this.http.put('http://localhost:8081/api/recipes/save', body, {headers: headers});
  }

  //
  // fetchData() {
  //   return this.http.get('http://localhost:8081/api/recipes')
  //     .map((response: Response) => response.json())
  //     .subscribe(
  //       (data: Recipe[]) => {
  //         console.log(data);
  //         this.recipes = data;
  //       }
  //     );
  // }

  fetchData() {
    return this.http.get('http://localhost:8081/api/recipes')
      .map((response: Response) => response.json())
      .subscribe(
        (data: Recipe[]) => {
          this.recipes = data;
          this.recipesChanged.emit(this.recipes);
        }
      );
  }


}
